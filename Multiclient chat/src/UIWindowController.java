import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class UIWindowController {
	
	@FXML
	Button sendButton;
	
	@FXML
	TextField messageField, nombreField;
	
	@FXML
	VBox viewVbox;
	
	String nombre;
	Client client;
	
	@FXML
	public void initialize() throws UnknownHostException, IOException {
		client = new Client();
	}
	
	@FXML
	private void setName() {
		nombre = nombreField.getText();
	}
	
	@FXML
	private void sendMessage() throws IOException {
		if(!messageField.getText().isEmpty()) {
			String message = messageField.getText();
			Label msg = new Label(message);
			client.sendMessage(message);
			
			messageField.clear();
		}
	}
	
	private class Client {
		public static final String IP = "224.0.0.251";
		public static final int port = 9000;
		InetAddress group;
		
		MulticastSocket socket;
		
		public Client() throws IOException {
			group = InetAddress.getByName(IP);
			socket = new MulticastSocket(port);
			socket.setTimeToLive(1);
			socket.joinGroup(group);
			new ReaderThread().start();
		}
		
		public void sendMessage(String message) throws IOException { 
			String finalMsg = nombre + ": " + message;
			byte[] buffer = finalMsg.getBytes(); 
			DatagramPacket datagram = new
			DatagramPacket(buffer,buffer.length,group,port); 
			socket.send(datagram); 
		}
		
		private class ReaderThread extends Thread {
			private static final int MAX_LEN = 1000; 
			
			public void run() { 
				while(true) { 
						byte[] buffer = new byte[ReaderThread.MAX_LEN]; 
						DatagramPacket datagram = new
						DatagramPacket(buffer,buffer.length,group,port); 
						String message; 
					try{ 
						socket.receive(datagram); 
						message = new
						String(buffer,0,datagram.getLength(),"UTF-8"); 
						Label label = new Label(message);
						Platform.runLater(new Runnable() {
							public void run() {
								viewVbox.getChildren().add(label);
							}
						});
					} catch(IOException e) { 
						System.out.println("Socket closed!"); 
					} 
				} 
			} 
		}
		
		
		
	}

}
